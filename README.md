## Synopsis

Coding exercise

## Code Example

public BatchProcessor(String[] args) {
    setValidatedFiles(new LinkedHashMap<>(args.length));
    
    Arrays.stream(args).forEach(s -> {
        File file = new File(s);
        if (file.exists()) {
            getValidatedFiles().put(file.getName(), file.getAbsolutePath());
        }
    });

    CountDownLatch countDownLatch = new CountDownLatch(getValidatedFiles().entrySet().size());
    
    getValidatedFiles().entrySet().forEach(s -> getExecutorService().submit(new LineReader(getExecutorService(), countDownLatch, s, this::batchLine)));

    try {
        countDownLatch.await();
        getExecutorService().shutdown();
        getExecutorService().awaitTermination(Integer.MAX_VALUE, TimeUnit.SECONDS);
    } catch (InterruptedException e) {
        logger.error("Error waiting for LineReaders to complete", e);
    }
    
}


## Installation
- To build a jar that includes all the dependencies:
./gradlew fatjar

- To run the jar:
java -jar build/libs/batchtranslate-all.jar {one or more paths to files to translate}

