package com.demo.batchtranslate.batch;

import org.junit.Before;
import org.junit.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

import static org.junit.Assert.*;

/**
 * Created by ryanwheeler on 5/18/17.
 */
public class BatchProcessorTest {
    private final Logger logger = LoggerFactory.getLogger(BatchProcessorTest.class);
    private final String expectedString = "En blå pictionary";
    private final String TEST_JSON = String.format("[[[\"%s\",\"one blue pictionary\",null,null,3]],null,\"en\"]", expectedString);
    private final ThreadLocal<Matcher> DOUBLE_QUOTE_MATCHER = ThreadLocal.withInitial(() -> Pattern.compile("\"").matcher(""));

    
    @Before
    public void setUp() throws Exception {

    }

    @Test
    public void testParse() throws Exception {
        StringBuilder sb = new StringBuilder(TEST_JSON.length());
        sb.append(TEST_JSON);
        Matcher matcher = DOUBLE_QUOTE_MATCHER.get();
        matcher.reset(sb);
        matcher.find();
        int beginFirstString = matcher.end();
        matcher.find();
        int endFirstString = matcher.start();

        String translation = sb.substring(beginFirstString, endFirstString);
        assertTrue(translation.equalsIgnoreCase(expectedString));
        logger.info(translation);
    }

}