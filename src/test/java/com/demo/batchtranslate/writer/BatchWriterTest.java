package com.demo.batchtranslate.writer;

import com.demo.batchtranslate.batch.OrderedBatch;
import org.junit.Before;
import org.junit.Test;

import java.io.BufferedWriter;

import static org.junit.Assert.*;
import static org.mockito.Mockito.*;

/**
 * Created by ryanwheeler on 5/17/17.
 */
public class BatchWriterTest {
    BufferedWriter bufferedWriter = mock(BufferedWriter.class);
    String[] words = {"one", "two", "three"};
    OrderedBatch ob0 = new OrderedBatch(words, 9);
    OrderedBatch ob1 = new OrderedBatch(words, 2);
    OrderedBatch ob2 = new OrderedBatch(words, 8);
    OrderedBatch ob3 = new OrderedBatch(words, 3);
    OrderedBatch ob4 = new OrderedBatch(words, 7);
    OrderedBatch ob5 = new OrderedBatch(words, 4);
    OrderedBatch ob6 = new OrderedBatch(words, 6);
    OrderedBatch ob7 = new OrderedBatch(words, 5);
    OrderedBatch ob8 = new OrderedBatch(words, 1);
    OrderedBatch[] batches = { ob0, ob1, ob2, ob3, ob4, ob5, ob6, ob7, ob8 };
    private BatchWriter batchWriter;
    
    @Before
    public void setUp() throws Exception {
        batchWriter = new BatchWriter("test", bufferedWriter);
    }

    @Test
    public void write() throws Exception {
        for (int i = 0; i < 8; i++) {
            batchWriter.write(batches[i]);
            verify(bufferedWriter, never()).write(anyString());
        }
        batchWriter.write(batches[8]);
        verify(bufferedWriter, times(9)).write(anyString());
    }

}