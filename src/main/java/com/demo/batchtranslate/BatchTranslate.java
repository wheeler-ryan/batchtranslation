package com.demo.batchtranslate;

import com.demo.batchtranslate.batch.BatchProcessor;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.Arrays;
import java.util.stream.Collectors;

/**
 * Created by ryanwheeler on 5/17/17.
 */
public class BatchTranslate {
    private static final Logger logger = LoggerFactory.getLogger(BatchTranslate.class);
    
    public static void main(String[] args) {
        if (args.length > 0) {
            String paths = Arrays.stream(args).collect(Collectors.joining(", "));
            logger.info("Translating " +  paths);
            BatchProcessor batchProcessor = new BatchProcessor(args);
        } else {
            logger.error("BatchTranslate requires one or more paths to files to translate");
        }
    }
}
