package com.demo.batchtranslate.batch;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.Map;
import java.util.concurrent.Callable;
import java.util.concurrent.CountDownLatch;
import java.util.concurrent.ExecutorService;

/**
 * Created by ryanwheeler on 5/17/17.
 */
public class LineReader implements Callable<FileSummary> {
    private final Logger logger = LoggerFactory.getLogger(getClass());
    private Integer lineNumber = 0;
    private ExecutorService executorService;
    private CountDownLatch countDownLatch;
    private Map.Entry<String, String> entry;
    private Handler<LineRead> lineReadHandler;

    public LineReader(ExecutorService executorService, CountDownLatch countDownLatch, Map.Entry<String, String> entry, Handler<LineRead> lineReadHandler) {
        this.executorService = executorService;
        this.countDownLatch = countDownLatch;
        this.entry = entry;
        this.lineReadHandler = lineReadHandler;
    }

    @Override
    public FileSummary call() {
        try {
            Files.lines(Paths.get(getEntry().getValue())).forEach(s -> {
                lineNumber++;
                getLineReadHandler().handle(new LineRead(getLineNumber(), getEntry().getKey(), s));
            });
        } catch (IOException e) {
            logger.error("Error reading lines from file", e);
        } finally {
            getCountDownLatch().countDown();
            return new FileSummary(entry.getKey(), lineNumber);
        }
    }

    public Integer getLineNumber() {
        return lineNumber;
    }

    public void setLineNumber(Integer lineNumber) {
        this.lineNumber = lineNumber;
    }

    public ExecutorService getExecutorService() {
        return executorService;
    }

    public void setExecutorService(ExecutorService executorService) {
        this.executorService = executorService;
    }

    public CountDownLatch getCountDownLatch() {
        return countDownLatch;
    }

    public void setCountDownLatch(CountDownLatch countDownLatch) {
        this.countDownLatch = countDownLatch;
    }

    public Map.Entry<String, String> getEntry() {
        return entry;
    }

    public void setEntry(Map.Entry<String, String> entry) {
        this.entry = entry;
    }

    public Handler<LineRead> getLineReadHandler() {
        return lineReadHandler;
    }

    public void setLineReadHandler(Handler<LineRead> lineReadHandler) {
        this.lineReadHandler = lineReadHandler;
    }
}
