package com.demo.batchtranslate.batch;

/**
 * Created by ryanwheeler on 5/17/17.
 */
public interface Handler<T> {
     void handle(T toHandle);
}
