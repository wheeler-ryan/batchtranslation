package com.demo.batchtranslate.batch;

/**
 * Created by ryanwheeler on 5/17/17.
 */
public class LineRead {
    Integer lineNumber;
    String fileName;
    String lineToTranslate;
    String lineTranslated;

    public LineRead(Integer lineNumber, String fileName, String lineToTranslate) {
        this.lineNumber = lineNumber;
        this.fileName = fileName;
        this.lineToTranslate = lineToTranslate;
    }

    public Integer getLineNumber() {
        return lineNumber;
    }

    public void setLineNumber(Integer lineNumber) {
        this.lineNumber = lineNumber;
    }

    public String getFileName() {
        return fileName;
    }

    public void setFileName(String fileName) {
        this.fileName = fileName;
    }

    public String getLineToTranslate() {
        return lineToTranslate;
    }

    public void setLineToTranslate(String lineToTranslate) {
        this.lineToTranslate = lineToTranslate;
    }

    public String getLineTranslated() {
        return lineTranslated;
    }

    public void setLineTranslated(String lineTranslated) {
        this.lineTranslated = lineTranslated;
    }
}
