package com.demo.batchtranslate.batch;

/**
 * Created by ryanwheeler on 5/17/17.
 */
public class FileSummary {
    String fileName;
    Integer lastLineNumber;

    public FileSummary(String fileName, Integer lastLineNumber) {
        this.fileName = fileName;
        this.lastLineNumber = lastLineNumber;
    }

    public String getFileName() {
        return fileName;
    }

    public void setFileName(String fileName) {
        this.fileName = fileName;
    }

    public Integer getLastLineNumber() {
        return lastLineNumber;
    }

    public void setLastLineNumber(Integer lastLineNumber) {
        this.lastLineNumber = lastLineNumber;
    }
}
