package com.demo.batchtranslate.batch;

import java.util.Comparator;

/**
 * Created by ryanwheeler on 5/17/17.
 */
public class OrderedBatch {
    String[] words;
    Integer lineNumber;
    
    public static Comparator<OrderedBatch> comparator() {
        return (o1, o2) -> (o1 instanceof OrderedBatch && o2 instanceof OrderedBatch) ? Integer.compare(o1.getLineNumber(), o2.getLineNumber()): 0;
    }

    public OrderedBatch(String[] words, Integer lineNumber) {
        this.words = words;
        this.lineNumber = lineNumber;
    }

    public String[] getWords() {
        return words;
    }

    public void setWords(String[] words) {
        this.words = words;
    }

    public Integer getLineNumber() {
        return lineNumber;
    }

    public void setLineNumber(Integer lineNumber) {
        this.lineNumber = lineNumber;
    }
}
