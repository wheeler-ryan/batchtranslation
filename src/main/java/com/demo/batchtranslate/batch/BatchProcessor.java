package com.demo.batchtranslate.batch;

import com.demo.batchtranslate.writer.BatchWriter;
import com.demo.batchtranslate.writer.Writer;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.BufferedReader;
import java.io.File;
import java.io.InputStreamReader;
import java.io.UnsupportedEncodingException;
import java.net.URL;
import java.net.URLConnection;
import java.net.URLEncoder;
import java.nio.charset.Charset;
import java.nio.charset.StandardCharsets;
import java.util.*;
import java.util.concurrent.*;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.stream.Collectors;

/**
 * Created by ryanwheeler on 5/17/17.
 */
public class BatchProcessor {
    protected final Logger logger = LoggerFactory.getLogger(getClass());
    private static final String RESPONSE_DELIMITER = " ";
    private static final String AS_YOU_GO = "AsYouGo.txt";
    private static final String BATCHED = "Batched.txt";
    private static final String TRANSLATE_URL = "https://translate.googleapis.com/translate_a/single?client=gtx&sl=en&tl=sv&dt=t&q=";
    private static final ThreadLocal<Matcher> DOUBLE_QUOTE_MATCHER = ThreadLocal.withInitial(() -> Pattern.compile("\"").matcher(""));
    private ExecutorService executorService = Executors.newWorkStealingPool(5);
    private Map<Integer, LinkedHashMap<String,LineRead>> lineNumberToLineMap = new HashMap<>();
    private Map<String, String> validatedFiles;
    private Map<String, Future<FileSummary>> fileSummaryMap;
    private Writer writer;
    private BatchWriter batchWriter;
    private List<String> outputPaths = new ArrayList(2);
    private long batchCount = -1;
    
    
    public BatchProcessor(String[] args) {
        setValidatedFiles(new LinkedHashMap<>(args.length));
        setFileSummaryMap(new HashMap<>(args.length));
        
        Arrays.stream(args).forEach(s -> {
            File file = new File(s);
            if (file.exists()) {
                getValidatedFiles().put(file.getName(), file.getAbsolutePath());
            }
        });
        
        if (getValidatedFiles().keySet().size() > 0) {
            
            try {
                
                setUpWriters();
                CountDownLatch countDownLatch = new CountDownLatch(getValidatedFiles().entrySet().size());

                getValidatedFiles().entrySet().forEach(s -> {
                    getFileSummaryMap().put(s.getKey(), getExecutorService().submit(new LineReader(getExecutorService(), countDownLatch, s, this::batchLine)));
                });
                countDownLatch.await();
                
                batchCount = getFileSummaryMap().values().stream().map(fileSummaryFuture -> {
                    long lastLineNumber = 0;
                    try {
                        lastLineNumber = fileSummaryFuture.get().getLastLineNumber();
                    } catch (Exception e) {
                        logger.error("Error retrieving FileSummary from future", e);
                    } finally {
                        return lastLineNumber;
                    }
                }).max(Long::compareTo).get();
                
                getExecutorService().shutdown();
                getExecutorService().awaitTermination(Short.MAX_VALUE, TimeUnit.SECONDS);
   
                closeWriters();
                logger.info("Translation complete");
                logger.info("Results written to " + getOutputPaths().stream().collect(Collectors.joining(" and ")));

            } catch (Exception e) {
                logger.error("Error waiting for LineReaders to complete", e);
            }
        }
    }

    private void setUpWriters() throws Exception {
        writer = new Writer(AS_YOU_GO);
        getOutputPaths().add(writer.getFilePath());
        batchWriter = new BatchWriter(BATCHED);
        getOutputPaths().add(batchWriter.getFilePath());
    }
    
    private void writeBatch(OrderedBatch orderedBatch) {
        getWriter().write(orderedBatch);
        getBatchWriter().write(orderedBatch);
    }
    
    private void closeWriters() {
        getWriter().close();
        getBatchWriter().close();
    }

    public void batchLine(LineRead lineRead) {
        logger.debug(lineRead.getFileName() + " " + lineRead.getLineNumber() + " " + lineRead.getLineToTranslate());
        Integer lineNumber = lineRead.getLineNumber();
        LinkedHashMap fileToLineMap = getLineNumberToLineMap().get(lineNumber);
        if (null == fileToLineMap) {
            fileToLineMap = new LinkedHashMap(getValidatedFiles().keySet().size());
            getLineNumberToLineMap().put(lineNumber, fileToLineMap);
        }
        fileToLineMap.put(lineRead.getFileName(), lineRead);
        // TODO this only works if all the files have the same number of lines
        if (fileToLineMap.keySet().size() == getValidatedFiles().keySet().size()) { 
            getLineNumberToLineMap().remove(lineRead.getFileName());
            LinkedHashMap finalFileToLineMap = fileToLineMap;
            CompletableFuture.supplyAsync(() -> translateBatch(finalFileToLineMap), getExecutorService());
        }
    }
    
    public Map<String, LineRead> translateBatch(Map<String, LineRead> batchToTranslate) {
        String toTranslate = batchToTranslate.values().stream().map(LineRead::getLineToTranslate).collect(Collectors.joining(RESPONSE_DELIMITER));
        String rawTranslation = callApi(toTranslate);
        String translationOnly = parseResponse(rawTranslation);
        String[] words = translationOnly.split(RESPONSE_DELIMITER);
        LineRead lineRead = batchToTranslate.values().iterator().next();
        Integer lineNumber = lineRead.getLineNumber();
        OrderedBatch orderedBatch = new OrderedBatch(words, lineNumber);
        writeBatch(orderedBatch);
        return batchToTranslate;
    }

    public String callApi(String toTranslate) {
        logger.debug("callApi " + toTranslate);
        String translated = "";
        try {
            String encoded = URLEncoder.encode(toTranslate, StandardCharsets.UTF_8.name());
            boolean success = false;
            int maxRetries = 3;
            int attempts = 0;
            if (null != toTranslate && !toTranslate.trim().isEmpty()) {
                while (!success && attempts < maxRetries) {

                    attempts++;

                    try {

                        URLConnection connection = new URL(TRANSLATE_URL + encoded).openConnection();
                        connection.setRequestProperty("User-Agent", "Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.11 (KHTML, like Gecko) Chrome/23.0.1271.95 Safari/537.11");
                        connection.setRequestProperty("Accept-Charset", StandardCharsets.UTF_8.name());
                        connection.setReadTimeout(5000);
                        connection.setConnectTimeout(8000);
                        translated = new BufferedReader(new InputStreamReader(connection.getInputStream(), Charset.forName(StandardCharsets.UTF_8.name()))).lines().collect(Collectors.joining("\n"));
                    } catch (Exception e) {
                        logger.error("Error", e);
                    }
                }

            }
        } catch (UnsupportedEncodingException e) {
            logger.error("Error encoding source: " + toTranslate, e);
        }
        return translated;
//        return "[[[\""+ toTranslate + "\",\"one blue pictionary\",null,null,3]],null,\"en\"]";
    }

    public String parseResponse(String rawResponse) {
        String response = "";
        StringBuilder sb = new StringBuilder(rawResponse.length());
        sb.append(rawResponse);
        Matcher matcher = DOUBLE_QUOTE_MATCHER.get();
        matcher.reset(sb);
        if (matcher.find()) {
            int beginFirstString = matcher.end();
            matcher.find();
            int endFirstString = matcher.start();
            response = sb.substring(beginFirstString, endFirstString);
        }

        return response;
    }
    
    
    
    public ExecutorService getExecutorService() {
        return executorService;
    }

    public void setExecutorService(ExecutorService executorService) {
        this.executorService = executorService;
    }

    public Map<Integer, LinkedHashMap<String, LineRead>> getLineNumberToLineMap() {
        return lineNumberToLineMap;
    }

    public void setLineNumberToLineMap(Map<Integer, LinkedHashMap<String, LineRead>> lineNumberToLineMap) {
        this.lineNumberToLineMap = lineNumberToLineMap;
    }

    public Map<String, String> getValidatedFiles() {
        return validatedFiles;
    }   

    public void setValidatedFiles(Map<String, String> validatedFiles) {
        this.validatedFiles = validatedFiles;
    }

    public Map<String, Future<FileSummary>> getFileSummaryMap() {
        return fileSummaryMap;
    }

    public void setFileSummaryMap(Map<String, Future<FileSummary>> fileSummaryMap) {
        this.fileSummaryMap = fileSummaryMap;
    }

    public BatchWriter getBatchWriter() {
        return batchWriter;
    }

    public void setBatchWriter(BatchWriter batchWriter) {
        this.batchWriter = batchWriter;
    }

    public Writer getWriter() {
        return writer;
    }

    public void setWriter(Writer writer) {
        this.writer = writer;
    }

    public List<String> getOutputPaths() {
        return outputPaths;
    }

    public void setOutputPaths(List<String> outputPaths) {
        this.outputPaths = outputPaths;
    }
}
