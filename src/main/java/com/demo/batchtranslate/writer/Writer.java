package com.demo.batchtranslate.writer;

import com.demo.batchtranslate.batch.OrderedBatch;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.Arrays;
import java.util.stream.Collectors;

/**
 * Created by ryanwheeler on 5/17/17.
 */
public class Writer {
    protected final Logger logger = LoggerFactory.getLogger(getClass());
    private String fileName;
    private String filePath;
    private BufferedWriter writer;
    boolean firstBatch = true;
    
    public Writer(String fileName) throws IOException {
        this.fileName = fileName;
        this.filePath = System.getProperty("user.dir") + File.separator + fileName;
        writer = new BufferedWriter(new FileWriter(filePath));
    }
    
    public Writer(String fileName, BufferedWriter bufferedWriter) {
        this.fileName = fileName;
        this.writer = bufferedWriter;
    }
    
    public void write(OrderedBatch orderedBatch) {
        synchronized (writer) {
            boolean newLine = !firstBatch;
            if (firstBatch) {
                firstBatch = false;
            }
            writeLine(orderedBatch.getWords(), newLine);
        }
    }
    
    protected void writeLine(String[] words, boolean newLine) {
        try {
            if (newLine) {
                getWriter().newLine();
            }
            getWriter().write(getWords(words));
        } catch (IOException e) {
            logger.error("Error writing to " + fileName);
        }
    }
    
    private String getWords(String[] words) {
        return Arrays.stream(words).collect(Collectors.joining(System.lineSeparator()));
    }

    public BufferedWriter getWriter() {
        return writer;
    }

    public void setWriter(BufferedWriter writer) {
        this.writer = writer;
    }

    public void close() {
        try {
            getWriter().close();
        } catch (IOException e) {
            logger.error("Error closing writer " + getClass().getSimpleName());
        }
    }

    public String getFilePath() {
        return filePath;
    }
}
