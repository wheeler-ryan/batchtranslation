package com.demo.batchtranslate.writer;

import com.demo.batchtranslate.batch.OrderedBatch;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.BufferedWriter;
import java.io.IOException;
import java.util.Arrays;
import java.util.PriorityQueue;
import java.util.Queue;

/**
 * Created by ryanwheeler on 5/17/17.
 */
public class BatchWriter extends Writer {
    protected final Logger logger = LoggerFactory.getLogger(getClass());
    private Queue<OrderedBatch> queue = new PriorityQueue<>(OrderedBatch.comparator());
    private int currentLineNumber = 1;

    public BatchWriter(String fileName) throws IOException {
        super(fileName);
    }
    
    public BatchWriter(String fileName, BufferedWriter bufferedWriter) {
        super(fileName, bufferedWriter);
    }

    @Override
    public void write(OrderedBatch orderedBatch) {
        synchronized (queue) {
            logger.debug("batchwriter " + orderedBatch.getLineNumber());
            queue.add(orderedBatch);
        }
        writeIfAvailable();
    }
    
    private void writeIfAvailable() {
        boolean tryNextLine = true;
        synchronized (queue) {
            while (!queue.isEmpty() && tryNextLine) {
                OrderedBatch nextBatch = queue.remove();
                if (null != nextBatch && nextBatch.getLineNumber() == currentLineNumber) {
                    writeToFile(nextBatch);
                    currentLineNumber++;
                } else {
                    tryNextLine = false;
                    queue.add(nextBatch);
                }

            }
        }
    }

    private void writeToFile(OrderedBatch nextBatch) {
        logger.debug("batch no : " + nextBatch.getLineNumber());
        boolean newLine = nextBatch.getLineNumber() != 1;
        String[] words = nextBatch.getWords();
        Arrays.sort(words);
        writeLine(words, newLine);
    }

    public int getCurrentLineNumber() {
        return currentLineNumber;
    }
}
